import React from 'react';
import PropTypes from 'prop-types';
import xlsx from 'xlsx';
import { Alert } from 'antd';
import csv from 'csv';
import { Upload, Icon } from 'antd';

export default class UploadExcel extends React.PureComponent {
   constructor(props){
      super(props);
      
      this.state = {};
   }
   
   validateFileFormat = format =>{
      return format === 'xlsx' || format === 'xls';
   };
   
   handleExcelParse = (fileDetails, fileLists) =>{
      this.props.onStepLoading(0);
      const file = fileLists[0];
      const fileName = file.name.split('.');
      const fileType = fileName[fileName.length - 1];
      const reader = new FileReader();
      console.log(this.validateFileFormat(fileType));
      if (!this.validateFileFormat(fileType)) {
         this.props.onStepLoading(null);
         this.props.onError({ type: "error", message: "The provided format is wrong, it should be .xlsx or .xls" });
         console.log("Here here here");
         return false;
      }
      
      reader.readAsBinaryString(file);
      reader.onload = () =>{
         const wb = xlsx.read(reader.result, { type: 'binary' });
         const rows = xlsx.utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]], {});
         console.log(rows);
         const headers = Object.keys(rows[0]);
         
         if (headers.length === 0) {
            this.props.onStepLoading(null);
            this.props.onError({ type: "error", message: "There is no header in this excel file." });
            return (<Alert showIcon={false} message="File Format is Wrong" banner/>);
         }
         
         this.props.onStepLoading(null);
         this.props.setExcelHeaders(headers);
         return true;
      };
      
      // console.log(fileName);
      // console.log(fileType);
      // console.log(fileDetails);
      // console.log(file);
   };
   
   render(){
      return (<div className="dropbox">
         <Upload.Dragger
            showUploadList={false}
            beforeUpload={this.handleExcelParse}
            multiple={false}
            name="files">
            <p className="ant-upload-drag-icon">
               <Icon type="inbox"/>
            </p>
            <p className="ant-upload-text">Click or drag file to this area to upload</p>
            <p className="ant-upload-hint">Supports excel files.</p>
         </Upload.Dragger>
      </div>);
   }
}

UploadExcel.propTypes = {
   onStepLoading: PropTypes.func.isRequired,
   onError: PropTypes.func.isRequired,
   setExcelHeaders: PropTypes.func.isRequired
};