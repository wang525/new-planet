import React from 'react';
import { Steps, Alert, Icon, Button } from 'antd';
import Header from 'components/header';
import UploadExcel from './uploadExcel/UploadExcel';
import './styles/factoryTemplate.scss';

export default class FactoryTemplate extends React.Component {
   constructor(props){
      super(props);
      
      this.state = {
         step: 0,
         error: null,
         loadingStepIndex: null,
         excelHeaders: []
      };
   }
   
   handleOnStepLoading = step =>{
      this.setState({ loadingStepIndex: step });
   };
   
   setStep = (step) => this.setState({ step, error: null });
   
   handleOnError = ({ type, message }) => this.setState({ error: { type, message } });
   
   setExcelHeaders = (excelHeaders) =>{
      this.setState({ excelHeaders }, () => this.setStep(1));
   };
   
   renderHeader = () =>{
      const { id, history } = this.props;
      return (<div className="display-flex space-between">
         <h3> Create excel template </h3>
         
         <Button type="default" onClick={() => history.push(`/factories/${id}`)}>
            <Icon type="arrow-left" theme="outlined"/>
            Back To Factories
         </Button>
      </div>);
   };
   
   renderSteps = () =>{
      const { loadingStepIndex, step } = this.state;
      
      return (<Steps current={step}>
         <Steps.Step
            title="Select File"
            description="Select a sample company file."
            icon={loadingStepIndex === 0 ? <Icon type="loading"/> : null}/>
         
         <Steps.Step
            title="Select Headers"
            description="Select file headers."
            icon={loadingStepIndex === 1 ? <Icon type="loading"/> : null}/>
         
         <Steps.Step
            title="Match Fields"
            description="Match headers with database."
            icon={loadingStepIndex === 2 ? <Icon type="loading"/> : null}/>
         
         <Steps.Step
            title="Complete"
            icon={loadingStepIndex === 3 ? <Icon type="loading"/> : null}/>
      </Steps>)
   };
   
   renderStepComponent = () =>{
      const { step } = this.state;
      
      switch (step) {
         case 0:
            return <UploadExcel
               onError={this.handleOnError}
               setExcelHeaders={this.setExcelHeaders}
               onStepLoading={this.handleOnStepLoading}/>;
         case 1:
            return <p> Select headers </p>;
         default:
            return false;
      }
   };
   
   render(){
      const { error } = this.state;
      
      return (<div className="page page-factory-template">
         <Header
            title={this.renderHeader()}
            description={this.renderSteps()}/>
         
         {error && <Alert showIcon
                          type="error"
                          message={error.message}
                          banner closeText="Close"/>}
         
         <div className="page-content">
            {this.renderStepComponent()}
         </div>
      </div>);
   }
}