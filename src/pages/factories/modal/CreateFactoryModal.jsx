import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Row, Col } from 'antd';
import FactoryForm from './../../factory/form/FactoryForm';

export default class CreateFactoryModal extends React.Component {
   static propTypes = {
      visible: PropTypes.bool.isRequired
   };
   
   render(){
      const { visible, onVisibilityChange } = this.props;
      
      return (<Modal
         title="Create Factory"
         onCancel={onVisibilityChange}
         visible={visible}>
         <Row>
            <Col spa={24}>
               <FactoryForm data={{}}/>
            </Col>
         </Row>
      </Modal>);
   }
}