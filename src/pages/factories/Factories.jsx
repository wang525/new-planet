import React from 'react';
import { Button } from 'antd';
import { compose, graphql } from 'react-apollo';
import gql from 'graphql-tag';
import Header from 'components/header';
import Toolbar from 'components/toolbar';
import FactoryGrid from './grid/FactoryGrid';
import CreateFactoryModal from './modal/CreateFactoryModal';

// import PropTypes from 'prop-types';

class Factories extends React.Component {
   constructor(props){
      super(props);
      
      this.state = {
         selectedRow: null,
         visibleModal: false
      }
   }
   
   handleSelectedRow = selectedRow => this.setState({ selectedRow });
   
   changeModalVisibility = () => this.setState({ visibleModal: !this.state.visibleModal });
   
   factoryHeader = () => (<div className="display-flex space-between">
      <h3> Factories </h3>
      <Button type="primary" onClick={this.changeModalVisibility}> Create Factory </Button>
   </div>);
   
   render(){
      const { data, history } = this.props;
      const { selectedRow, visibleModal } = this.state;
      
      return (<div className="page page-factories">
         <Header
            title={this.factoryHeader()}
            description="You can Create, Edit or Remove factories in this page"/>
         <div className="page-content">
            <Toolbar>
               <Button
                  onClick={() => history.push(`/factories/${selectedRow.id}`)}
                  disabled={!selectedRow}
                  type="primary"> Edit </Button>
               <Button> Remove </Button>
               <Button> Active Orders </Button>
            </Toolbar>
            
            <FactoryGrid
               onRowSelect={this.handleSelectedRow}
               data={data}
               loading={data.loading}/>
            
            <CreateFactoryModal
               onVisibilityChange={this.changeModalVisibility}
               visible={visibleModal}/>
         </div>
      </div>)
   }
}

export default compose(graphql(
   gql`
       query  {
           factories {
               id
               name
           }
       }`
))(Factories);