export default [
   { headerName: "Factory Name", field: "name" },
   { headerName: "Model", field: "model" },
   { headerName: "Price", field: "price" }
];
