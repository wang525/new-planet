var DynamicsWebApi = require('dynamics-web-api');
 
/* the following settings should be taken from Azure for your application */
/* and stored in app settings file or in global variables */
 
/* OAuth Token Endpoint */
var authorityUrl = 'https://login.microsoftonline.com/00000000-0000-0000-0000-000000000011/oauth2/token';
/* CRM Organization URL */
var crmwebapi_host = 'https://myorg.crm.dynamics.com';
var crmwebapi_url = '/api/data/v9.0/';
/* Dynamics 365 Client Id when registered in Azure */
var clientId = '00000000-0000-0000-0000-000000000001';
var username = 'crm-user-name';
var password = 'crm-user-password';
var token = 'unspecified-token';
 
/* create DynamicsWebApi object */
var dynamicsWebApi = new DynamicsWebApi({
    webApiUrl: crmwebapi_host + crmwebapi_url,
    token: token
});
 
/* Definition DynamicsWebService. */
var DynamicsWebService = {
  getCustomers() {
    /* Assume that "customers" function exists. And it has ["id", "name"] fields. */
    /* Now We disable following codes because need to discuss this api with ... */
    return new Promise((resolve, reject) => {
      if (false) {
        dynamicsWebApi.retrieveAll("customers", ["id", "name"], "statecode eq 0")
        .then(function(response) {
          resolve(response.customers);
        })
      }
      /* Data For Test!!!*/
      else {
        resolve([
          {id: 1, name: 'John Maximov'},
          {id: 2, name: 'Julian Ibramobic'},
          {id: 3, name: 'Zoe Joy'},
          {id: 4, name: 'David Sam'}
        ]);
      }
    } )
  },

  createCustomer() {
    /* Not implemented yet */
  }
};

module.exports = DynamicsWebService;
