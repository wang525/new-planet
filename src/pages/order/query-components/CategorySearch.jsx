import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';

import { Query } from 'react-apollo';
import gql from 'graphql-tag';

const Option = Select.Option;

const QUERY = gql`
  query Category($term:String!) {
    categories(where: {
      name_contains:$term,
      parent: null
    }) 
    {
      id
      name
    }
  }
`;

/*const QUERY = gql`
  query {
    categories 
    {
      id
      name
    }
  }
`;*/

export default class CategorySearch extends React.Component {

  static propTypes = {
    term: PropTypes.string,
    onSearch:PropTypes.func.isRequired,
    onSelect:PropTypes.func.isRequired
  };

  static defaultProps = { term: '' };

  constructor(props){
    super(props);
    
  }

  searchTerm = (value) => {
    //console.log('term', value)
    this.props.onSearch(value);
  }

  selectedOption = (optionId) => {
    console.log('selectedOption', optionId)
    this.props.onSelect(optionId);    
  }

  render () {
    const {term} = this.props;

    return (<Query query={QUERY} variables={{term}}>
        {({ data, error, loading }) => {
          if (error) return '💩 Oops! '+error.message;
          
          return (
            <Select
                showSearch
                filterOption={false}
                size="large"               
                placeholder="Search for sport"
                onSearch={this.searchTerm}
                onSelect={this.selectedOption}
              >
                {data&&data.categories&&data.categories.map(sport => (
                  <Option key={sport.id}>{sport.name}</Option>
                ))}
              </Select>
          );
        }}
      </Query>)
  }
}