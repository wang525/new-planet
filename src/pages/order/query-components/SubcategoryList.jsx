import React from 'react';
import PropTypes from 'prop-types';
import { Select, Radio } from 'antd';

import { Query } from 'react-apollo';
import gql from 'graphql-tag';

const Option = Select.Option;



const QUERY = gql`
  query Category($term:ID!) {
    categories(where: {
      id: $term
    }){
      children{
        id
        name
      }
    }
  }
`;

/*const QUERY = gql`
  query {
    categories 
    {
      id
      name
    }
  }
`;*/

const radioStyle = {
  display: 'block',
  marginBottom:'8px'
};


export default class SubcategoryList extends React.Component {

  static propTypes = {
    term: PropTypes.string,
    onChange:PropTypes.func.isRequired    
  };

  static defaultProps = { term: '' };

  constructor(props){
    super(props);
    
  }
  
  onEnableNextButton = (e) => {
    console.log('EnableNextButton', e.target.value)
    this.props.onChange(e.target.value);   
  }
 
  render () {
    const {term} = this.props;
    return (<Query query={QUERY} variables={{term}}>
        {({ data, error, loading }) => {
          if (error) return '💩 Oops! '+error.message;

          return (
            <Radio.Group defaultValue="a" buttonStyle="solid" onChange={this.onEnableNextButton}>

            {data&&data.categories&&data.categories&&data.categories.map(sport => (
              sport.children.map(sub_sport => (
              <Radio.Button style={radioStyle} key={sub_sport.id} value={sub_sport.id}>{sub_sport.name}</Radio.Button>
              ))
            ))}
            </Radio.Group>
          );
        }}
      </Query>)
  }
}