import React from 'react';
import { compose, graphql } from 'react-apollo';
import { Row, Col, Card, Collapse, Panel, Button, Icon, Steps  } from 'antd';

import gql from 'graphql-tag';
import { Skeleton } from 'antd';
import Header from 'components/header';
import Loading from 'components/loading';
import CustomerForm from './form/CustomerForm';
import SportForm from './form/SportForm';
import './styles/order.scss';

const Step = Steps.Step;

const steps = [{
  title: 'Customer'
}, {
  title: 'Sport'
}, {
  title: 'Design'
}, {
  title: 'Share/Order'
}];

class Order extends React.Component {
 
  constructor(props){
    super(props);
    
   
    if (props.match.params.id=='create') {
      this.state = {
        data: {
          loading:false,
          order:{

          }
        },
        current:1,
        canGoNext: false
      }
    }
    
 }

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

   
   handleSubmit = (e) =>{
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) =>{
         if (!err) {
            console.log('Received values of form: ', values);
         }
      });
   };

   renderStepContent = (stepIndex) =>{
    const { data } = this.state;
     if (stepIndex==0) {
      return (
        <CustomerForm data={data.order}/>
      )
     }
     else if (stepIndex==1) {
      return (
        <SportForm data={data.order} onChangeNextButton={this.handleCustomerFormStatus}/>
      )
     }
   }
   
   handleCustomerFormStatus = (status) => {
    this.setState({
      canGoNext : status
    })
   }
   
   render(){
      const {  history } = this.props;
      const { data, current } = this.state;
      const Panel = Collapse.Panel;
      
      return (<div className="page page-order">
         {data.loading ?
          <Loading loading={data.loading}/> :
          <div>
             <Header
                /*title={
                   <div className="display-flex space-between">
                      <h3>Order - {data.order.name}</h3>
                      <Button type="default" onClick={() => history.push('/orders')}>
                         <Icon type="arrow-left" theme="outlined"/>
                         Back To Orders
                      </Button>
                   </div>
                }
                description="You can change order properties in this page"*/
                title={
                  <Steps current={current}>
                    {steps.map(item => <Step key={item.title} title={item.title} />)}
                  </Steps>
                }
                />
             <div className="page-content-padding">
                <Row gutter={16}>
                   <Col span={18} sm={24} xs={24} md={18} lg={18}>
                      
                      <Card className="no-border page-content">                                                
                        <div className="steps-content">{this.renderStepContent(current)}</div>
                        <div className="steps-action">
                        {
                          current < steps.length - 1
                          && <Button className="step-next-button" onClick={() => this.next()} disabled={!this.state.canGoNext}>Next</Button>
                        }
                        {
                          current === steps.length - 1
                          && <Button className="step-next-button" onClick={() => console.log('Done')}>Done</Button>
                        }
                        {
                          current > 0
                          && (
                            
                          <Button className="step-back-button" onClick={() => this.prev()}>
                            <Icon type="left" style={{ color: 'rgba(0,0,0,.25)' }}></Icon>
                            Back
                          </Button>
                          )
                        }
                      </div>
                      </Card>
                   </Col>
                </Row>
             </div>
          </div>
         }
      </div>);
   }
}

/*const GET_ORDER = gql`query ($id: ID!){
    order(where: { id: $id }) {
        id
        name
    }
}`;

export default compose(graphql(GET_ORDER,
   {
      options: (props) => (
         { variables: { id: props.match.params.id } }
      )
   }
))(Order);*/
export default Order;