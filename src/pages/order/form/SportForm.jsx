import React from 'react';
import { Form, Input, Button, Tooltip, Icon, Select, Radio } from 'antd';
import gql from "graphql-tag";
import { Query } from "react-apollo";

import CategorySearch from "../query-components/CategorySearch"
import SubcategoryList from "../query-components/SubcategoryList"
import PropTypes from 'prop-types';
import '../styles/order.scss';


const Option = Select.Option;
const testSportOptions = [{
  id: 'Customer',
  name: 'Customer'
}, {
  id: 'Developer',
  name: 'Developer'
}, {
  id: 'Worker',
  name: 'Worker'
}];

const GET_CATEGORIES = gql`
query  {
    categories {
        id
        name
    }
}`;

class SportForm extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    onChangeNextButton:PropTypes.func.isRequired
  };

  state = {
    categoryOptions: testSportOptions,
    categoryQuery:'',
    selectedCategoryId:'',
    selectedItem:0,
    nextButton: false
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  handleSportSearchChange = (value) => {
    this.setState({
      categoryQuery:value,
      selectedItem: 0
    })
    this.props.onChangeNextButton(false);
  }

  handleSportSelect = (sportId) => {
    this.setState({
      selectedCategoryId:sportId,
      selectedItem: 1
    })
    this.props.onChangeNextButton(false);
  }

  handleSelectedButton = (value) => {
    console.log('handleSelectedButton', value)
    console.log('selectedItem', this.state.selectedItem)
    if (value && this.state.selectedItem===1){
      this.state.nextButton = true;
    } else {
      this.state.nextButton = false;
    }
    this.props.onChangeNextButton(this.state.nextButton);
  }

  render() {
    const { data } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { categoryQuery } = this.state;
    const { selectedCategoryId } = this.state;
  
    const FormItem = Form.Item;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 4,
        },
      },
    };

    return (<Form
      onSubmit={this.handleSubmit}
      layout="horizontal">
      <div className="order-form-wrapper">
        <div className="order-form-title-wrapper">
          <label className="order-form-title">Create Design</label>
        </div>

        <div className="order-group-title-wrapper">
          <div className={this.state.selectedItem === 0 ? 'step-icon step-process': 'step-icon step-finish'} style={{ float: 'left', marginRight: '24px' }}>
            <span className="step-text">1</span>
          </div>
          <label className="order-group-title">Select Your Sport</label>
        </div>

        {/*Select Sport for Search*/}
        <div className="order-group-content-wrapper">
          <FormItem
            {...formItemLayout}
          >
            {getFieldDecorator('sport', {
              rules: [{ required: true, message: 'Please select sport!' }],
            })(
              <CategorySearch term={categoryQuery} onSearch={this.handleSportSearchChange} onSelect={this.handleSportSelect} />
              )}
          </FormItem>
        </div>

        {/*Selected Category List*/}
        <div className="order-group-title-wrapper">
          <div className={this.state.selectedItem === 0 ?  'step-icon step-wait':'step-icon step-process'} style={{ float: 'left', marginRight: '24px' }}>
            <span className="step-text">2</span>
          </div>
          <label className="order-group-title">&nbsp;</label>
        </div>
        <div className="order-group-content-wrapper">
          <div>
            <SubcategoryList term={selectedCategoryId} onChange={this.handleSelectedButton} />
          </div>
        </div>
      </div>


    </Form>);
  }
}

export default Form.create()(SportForm);
