import React from 'react';
import { Row, Col, Form, Input, Button, Tooltip, Icon, Select, AutoComplete, Mention, Steps } from 'antd';

import PropTypes from 'prop-types';
import '../styles/order.scss';
import DynamicsWebService from '../service/dynamics_web_service';

class CustomerForm extends React.Component {

  static propTypes = {
    data: PropTypes.object.isRequired
  };

  state = {
    autoCompleteResult: [],
    current: 0,
    customers: [],
  }

  constructor(props){
    super(props);
    
    DynamicsWebService.getCustomers().then((customers)=>{
      this.setState({customers: customers})
    });
 }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  handleCustomerChange = (value) => {
    let autoCompleteResult;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = ['-sport-A', '-sport-B', '-sport-C'].map(domain => `${value}${domain}`);
    }
    this.setState({ autoCompleteResult });
  }

  handleSelectChange = (value) => {

  }

  render() {
    const { data } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;
    const Option = Select.Option;
    const AutoCompleteOption = AutoComplete.Option;
    const FormItem = Form.Item;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 4,
        },
      },
    };

    const customerOptions = autoCompleteResult.map(sport => (
      <AutoCompleteOption key={sport}>{sport}</AutoCompleteOption>
    ));

    const { current, customers } = this.state

    return (<Form
      onSubmit={this.handleSubmit}
      layout="horizontal">
      <div className="order-form-wrapper">
        <div className="order-form-title-wrapper">
          <label className="order-form-title">Create Design</label>
        </div>

        <div className="order-group-title-wrapper">
          <div className="step-icon step-process" style={{ float: 'left', marginRight: '24px' }}>
            <span className="step-text">1</span>
          </div>
          <label className="order-group-title">Select Customer Account</label>
        </div>

        {/*Select Club*/}
        <div className="order-group-content-wrapper">
          <FormItem
            {...formItemLayout}
          >
            {getFieldDecorator('sport', {
              rules: [{ required: true, message: 'Please select club!' }],
            })(
              <Select
                showSearch
                filterOption={false}
                size="large"               
                placeholder="Rooster Professional Club"
                onSelect={this.handleCustomerChange}
              >
                {customers&&customers.map(customer => (
                  <Option key={customer.id}>{customer.name}</Option>
                ))}
              </Select>
              )}
          </FormItem>
        </div>

        <div className="order-group-title-wrapper">
          <div className="step-icon step-wait" style={{ float: 'left', marginRight: '24px' }}>
            <span className="step-text">2</span>
          </div>
          <label className="order-group-title">Select Account Contact (optional)</label>
        </div>

        <div className="order-group-content-wrapper">
          {/*Name*/}
          <label className="order-general-label">Name</label>
          <div className="order-input-sm">
            <FormItem
              {...formItemLayout}
            >
              {getFieldDecorator('afl', {
                rules: [{ required: true, message: 'Please insert your name' }],
              })(
                <AutoComplete
                  size="large"

                  dataSource={customerOptions}
                  onChange={this.handleCustomerChange}
                >
                  <Input
                    //prefix={<Icon style={{ color: 'rgba(0,0,0,.25)' }} /> } 
                    placeholder="Jeremy Webster" />
                </AutoComplete>
                )}
            </FormItem>
          </div>


          {/*Email*/}
          <label className="order-general-label">Email</label>
          <div className="order-input-sm">
            <FormItem
              {...formItemLayout}
            >
              {getFieldDecorator('afl', {
                rules: [{ required: true, message: 'Please insert your email' }],
              })(
                <AutoComplete
                  size="large"
                  dataSource={customerOptions}
                  onChange={this.handleCustomerChange}
                >
                  <Input
                    //prefix={<Icon style={{ color: 'rgba(0,0,0,.25)' }} /> } 
                    placeholder="j@hotmail.com" />
                </AutoComplete>
                )}
            </FormItem>
          </div>


        </div>

        <div className="order-section-title-wrapper" >
          <label >Design Notes (optional)</label>
        </div>

        {/*Textarea Mention */}
        <div className="order-textarea-wrapper">
          <FormItem
            {...formItemLayout}
          >
            <Mention
              style={{ width: '100%', height: 100, backgroundColor: '#d8d8d8' }}
              multiLines
            />
          </FormItem>
        </div>
      </div>

    </Form>);
  }
}

export default Form.create()(CustomerForm);
