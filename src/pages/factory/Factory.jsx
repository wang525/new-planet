import React from 'react';
import { compose, graphql } from 'react-apollo';
import { Row, Col, Card, Collapse, Panel, Button, Icon } from 'antd';
import gql from 'graphql-tag';
import { Skeleton } from 'antd';
import Header from 'components/header';
import Loading from 'components/loading';
import FactoryForm from './form/FactoryForm';
import FactoryExcelTemplates from './excelTemplates/ExcelTemplates';
import './styles/factory.scss';

class Factory extends React.Component {
   
   
   handleSubmit = (e) =>{
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) =>{
         if (!err) {
            console.log('Received values of form: ', values);
         }
      });
   };
   
   
   render(){
      const { data, history, match } = this.props;
      const Panel = Collapse.Panel;
      
      return (<div className="page page-factory">
         {data.loading ?
          <Loading loading={data.loading}/> :
          <div>
             <Header
                title={
                   <div className="display-flex space-between">
                      <h3>Factory - {data.factory.name}</h3>
                      <Button type="default" onClick={() => history.push('/factories')}>
                         <Icon type="arrow-left" theme="outlined"/>
                         Back To Factories
                      </Button>
                   </div>
                }
                description="You can change factory properties in this page"/>
             <div className="page-content page-content-padding">
                <Row gutter={16}>
                   <Col span={18} sm={24} xs={24} md={18} lg={18}>
                      <Card>
                         <FactoryForm data={data.factory}/>
                      </Card>
                   </Col>
                   <Col span={6} sm={24} xs={24} md={6} lg={6}>
                      <Card className="sidebar">
                         <Collapse bordered={false} defaultActiveKey={['1']}>
                            <Panel header="Email Template" key="1" className="panel">
                               <FactoryExcelTemplates data={data}/>
                               <Button
                                  block
                                  type="primary"
                                  onClick={() => history.push(`/factories/${match.params.id}/create-excel-template`)}>
                                  <Icon type="plus"/>
                                  Create/Edit factory Template
                               </Button>
                            </Panel>
                            <Panel header="Current Active Orders" key="2" className="panel">
                               <p> A List of current </p>
                            </Panel>
                            <Panel header="Other" key="3" className="panel">
                               <p> Other tools </p>
                            </Panel>
                         </Collapse>
                      </Card>
                   </Col>
                </Row>
             </div>
          </div>
         }
      </div>);
   }
}

const GET_FACTORY = gql`query ($id: ID!){
    factory(where: { id: $id }) {
        id
        name
        excel_templates {
            id
            name
            default
        }
    }
}`;

export default compose(graphql(GET_FACTORY,
   {
      options: (props) => (
         { variables: { id: props.match.params.id } }
      )
   }
))(Factory);