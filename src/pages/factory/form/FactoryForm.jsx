import React from 'react';
import { Form, Input, Button, Tooltip, Icon } from 'antd';
import PropTypes from 'prop-types';

class FactoryForm extends React.Component {
   static propTypes = {
      data: PropTypes.object.isRequired
   };
   
   
   handleSubmit = (e) =>{
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) =>{
         if (!err) {
            console.log('Received values of form: ', values);
         }
      });
   };
   
   render(){
      const { data } = this.props;
      const { getFieldDecorator } = this.props.form;
      
      const FormItem = Form.Item;
      const formItemLayout = {
         labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
         },
         wrapperCol: {
            xs: { span: 24 },
            sm: { span: 12 },
         },
      };
      
      const tailFormItemLayout = {
         wrapperCol: {
            xs: {
               span: 24,
               offset: 0,
            },
            sm: {
               span: 16,
               offset: 4,
            },
         },
      };
      
      return (<Form
         onSubmit={this.handleSubmit}
         layout="horizontal">
         
         <FormItem
            {...formItemLayout}
            label="Factory Name">
            {getFieldDecorator('name', {
               initialValue: data.name,
               rules: [
                  {
                     required: true, message: 'Please input a factory name!',
                  }
               ],
            })(
               <Input/>
            )}
         </FormItem>
         
         <FormItem
            {...formItemLayout}
            label="Factory Description">
            {getFieldDecorator('description', {
               initialValue: data.description,
            })(
               <Input/>
            )}
         </FormItem>
         
         <FormItem
            {...formItemLayout}
            label={(
               <span>
              Domain Address
                  <Tooltip title="Only a factory update email with the below domain are valid.">
                <Icon type="question-circle-o"/>
              </Tooltip>
            </span>
            )}>
            {getFieldDecorator('domain_address', {
               initialValue: data.domain_address,
            })(
               <Input/>
            )}
         </FormItem>
         
         <FormItem {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit"> Update </Button>
         </FormItem>
      </Form>);
   }
}

export default Form.create()(FactoryForm);
