import React from 'react';
import { List, Icon, Tooltip } from 'antd';
import PropTypes from 'prop-types';

export default class ExcelTemplates extends React.PureComponent {
   static  propTypes = {
      data: PropTypes.object.isRequired
   };
   
   setDefaultComponent = () =>{
   
   };
   
   renderListItem = template =>{
      return (<List.Item
         actions={
            [
               <Tooltip placement="topRight"
                        title={template.default ? "Default" : "Set as the default company template"}>
                  <Icon
                     type="check-circle"
                     theme={template.default ? "twoTone" : "outlined"}
                     onClick={this.setDefaultComponent}
                  />
               </Tooltip>
            ]}>
         <div>{template.name}</div>
      </List.Item>)
         ;
   };
   
   render(){
      const { data } = this.props;
      const { excel_templates = [] } = data.factory;
      excel_templates.sort(template => !template.default);
      
      return (<div className="excel-templates-list">
         <List
            loading={data.loading}
            dataSource={excel_templates}
            renderItem={this.renderListItem}/>
      </div>);
   }
}