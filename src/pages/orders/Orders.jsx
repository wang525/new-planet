import React from 'react';
import { Button } from 'antd';
import { compose, graphql } from 'react-apollo';
import gql from 'graphql-tag';
import Header from 'components/header';
import Toolbar from 'components/toolbar';
import OrderGrid from './grid/OrderGrid';

// import PropTypes from 'prop-types';

class Orders extends React.Component {
   constructor(props){
      super(props);
      
      this.state = {
         selectedRow: null,
         visibleModal: false
      }
   }
   
   handleSelectedRow = selectedRow => this.setState({ selectedRow });
   
   createOrder = () => {
      const { history } = this.props;
      history.push(`/orders/create`);
    };
   
   orderHeader = () => (<div className="display-flex space-between">
      <h3> Orders </h3>
      <Button type="primary" onClick={this.createOrder}> Create Order </Button>
   </div>);
   
   render(){
      const { data, history } = this.props;
      const { selectedRow, visibleModal } = this.state;
      
      return (<div className="page page-orders">
         <Header
            title={this.orderHeader()}
            description="You can Create, Edit or Remove orders in this page"/>
         <div className="page-content">
            <Toolbar>
               <Button
                  onClick={() => history.push(`/orders/${selectedRow.id}`)}
                  disabled={!selectedRow}
                  type="primary"> Edit </Button>
               <Button> Remove </Button>
               <Button> Active Orders </Button>
            </Toolbar>
            
            <OrderGrid
               onRowSelect={this.handleSelectedRow}
               data={data}
               loading={data.loading}/>
            
         </div>
      </div>)
   }
}

export default compose(graphql(
   gql`
       query  {
           orders {
               id
               name
           }
       }`
))(Orders);