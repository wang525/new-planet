import React from 'react';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import gridDefs from './gridDefs';
import Grid from 'components/grid';
import Loading from 'components/loading';

export default class OrderGrid extends React.Component {
   static propTypes = {
      loading: PropTypes.bool.isRequired,
      data: PropTypes.object.isRequired,
      onRowSelect: PropTypes.func.isRequired
   };
   
   componentDidUpdate(){
      this.setGridLoading();
   }
   
   setGridLoading = () =>{
      const { loading } = this.props;
      
      if (loading && this.gridApi) {
         this.gridApi.showLoadingOverlay();
      } else if (this.gridApi) {
         this.gridApi.hideOverlay();
      }
   };
   
   onGridReady = (event) =>{
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.gridApi.sizeColumnsToFit();
      this.setGridLoading();
   };
   
   handleSelectionChange = () =>{
      const selectedRows = this.gridApi.getSelectedRows();
      
      if (selectedRows.length > 0) {
         return this.props.onRowSelect(selectedRows[0]);
      } else {
         return this.props.onRowSelect(null);
      }
   };
   
   render(){
      const { data } = this.props;
      
      return (
         <Grid
            ref={this.grid}
            onGridReady={this.onGridReady}
            columnDefs={gridDefs}
            rowData={data.orders}
            onSelectionChanged={this.handleSelectionChange}
            suppressRowClickSelection={false}
            rowSelection="single"/>);
   }
}