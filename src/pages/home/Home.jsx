import React from "react";
import { Col, Row, Card, Button, Icon } from "antd";
import "./styles/home.scss";

export default class Home extends React.Component {
   componentWillMount(){
      document.title = "Portal Home";
   }
   
   heroHeader = () =>{
      return (
         <Row className="hero-header">
            <div className="overlay"/>
            
            <Col span={12} lg={12} md={12} sm={24} xs={24} smOffset={0}>
               <Card title="Welcome to Classic Staff Portal Prototype">
                  <p>
                     This will be your one stop shop for everything related to Classic.
                     Please note that this platform is currently in prototype. There
                     are many new features on the way and we invite you to provide your
                     feedback.
                  </p>
                  <br/>
                  <p>Sincerely, The Ezerus Team</p>
                  <Button
                     type="primary"
                     size="large"
                     style={{ float: "right" }}
                     onClick={() => document.getElementById("doorbell-button").click()}
                  >
                     Give Feedback / Get Help
                  </Button>
               </Card>
            </Col>
            
            <Col span={6} offset={5} lg={8} md={6} sm={24} xs={24} smOffset={0}>
               <Card title="Quick Access">
                  <div>
                     <Button
                        block
                        ghost
                        type="primary"
                        size="large">
                        <Icon type="shopping"/> Sales CRM
                     </Button>
                     
                     <Button
                        block
                        ghost
                        type="primary"
                        style={{ marginTop: "10px" }}
                        onClick={() => window.location.href = "https://classic.ezerus.com.au/"}
                        size="large">
                        <Icon type="question-circle"/> How To Guides
                     </Button>
                     
                     <Button
                        block
                        ghost
                        size="large"
                        style={{ marginTop: "10px" }}
                        onClick={() => window.location.href = "http://staffportal.classicsports.com.au/dyo/index.html"}
                        type="primary">
                        <Icon type="plus-circle"/> Quote Portal
                     </Button>
                  </div>
               </Card>
            </Col>
         </Row>
      );
   };
   
   render(){
      return <div className="homepage">
         {this.heroHeader()}
         {/*<Row>*/}
         {/*<Col sm={24} xs={24} md={12}>*/}
         {/*<Card title="calendar">*/}
         {/*<Calendar/>*/}
         {/*</Card>*/}
         {/*</Col>*/}
         {/*</Row>*/}
      </div>;
   }
}
