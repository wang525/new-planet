import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from "react-apollo";
import 'antd/dist/antd.css';
import Apollo from './lib/apollo';
import App from './App';
import "./styles/styles.scss";
import { BrowserRouter } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
   <BrowserRouter>
      <ApolloProvider client={Apollo.client}>
         <App/>
      </ApolloProvider>
   </BrowserRouter>,
   document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
