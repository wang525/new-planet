import gql from 'graphql-tag';
import { env } from "./../common";
import Auth0Lock from "auth0-lock";
import Apollo from "./../apollo";

const UPSERT_USER = gql`
    mutation($idToken: String!) {
        upsertUser(idToken: $idToken) {
            id
            email
            first_name
            last_name
            picture
        }
    }
`;

export default class Auth {
   constructor(cb){
      this.clietId = env("REACT_APP_AUTH0_CLIENT_ID");
      this.domain = env("REACT_APP_AUTH0_DOMAIN");
      this.callbackURL = env("REACT_APP_AUTH0_CALLBACK_URL");
      this.lock = new Auth0Lock(this.clietId, this.domain, {
         closable: false,
         languageDictionary: {
            title: "Classic Sports Portal"
         },
         theme: {
            logo:
               "https://cdn.zeplin.io/5b55ba3dc3dae88a079275f9/assets/2D9FC9F2-6015-4FF5-B526-2805BA247618.png"
         },
         auth: {
            redirectUrl: this.callbackURL,
            redirect: false,
            responseType: "token id_token",
            scope: "openid email",
            // sso: false, TODO: they should set their server up,
            connectionScopes: {
               connectionName: ["email", "profile", "openid"]
            }
         }
      });
      
      this.handleAuthentication();
      this.cb = cb;
   }
   
   
   login = () =>{
      if (!this.isAuthenticated()) {
         this.lock.show();
      }
   };
   
   static logout(){
      // Clear access token and ID token from local storage
      localStorage.removeItem('access_token');
      localStorage.removeItem('id_token');
      localStorage.removeItem('expires_at');
      //TODO: Send an http req to Auth0
      console.log("I'm here")
      window.location.reload();
   }
   
   handleAuthentication = () =>{
      this.lock.on('authenticated', this.setSession);
      
      this.lock.on('authorization_error', err =>{
         alert(`Error: ${err.error}. Check the console for further details.`);
         const data = { status: `error`, errMessage: err.error };
         this.cb(data);
      })
   };
   
   setSession = async authResult =>{
      if (authResult && authResult.accessToken && authResult.idToken) {
         let expiresAt = JSON.stringify(authResult.expiresIn * 1000 + new Date().getTime());
         
         localStorage.setItem('access_token', authResult.accessToken);
         localStorage.setItem('id_token', authResult.idToken);
         localStorage.setItem('expires_at', expiresAt);
         
         const data = {
            status: `success`,
            accessToken: authResult.accessToken,
            idToken: authResult.idToken,
            expiresAt
         };
         
         try {
            await this.upsertUser({ ...data });
            return window.location.href = "/";
         } catch (e) {
            throw new Error(e.message);
         }
      }
   };
   
   upsertUser = async ({ idToken }) =>{
      return Apollo
         .client
         .mutate({ mutation: UPSERT_USER, variables: { idToken } })
         .then(() => window.location.reload())
         .catch(e =>{
            console.log(e);
            return false;
         });
   };
   
   
   isAuthenticated = () =>{
      const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
      return new Date().getTime() < expiresAt;
   }
}