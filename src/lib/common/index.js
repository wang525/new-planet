export function env(key) { 
  return process.env[key] || {};
}