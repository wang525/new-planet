import ApolloClient from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ErrorLink } from 'apollo-link-error';

class ApolloClientProvider {
   constructor(){
      const httpLink = new HttpLink({
         uri: process.env.REACT_APP_GRAPHQL_URL
      });
      
      const middlewareLink = new ApolloLink((operation, forward) =>{
         operation.setContext({
            headers: {
               'token': `Brear ${localStorage.getItem("id_token")}`
            },
         });
         
         return forward(operation);
      });
      
      const errorLink = new ErrorLink(({ networkError, graphQLErrors }) =>{
         if (graphQLErrors) {
            console.log(graphQLErrors);
         }
         if (networkError) console.log(`[Network error]: ${networkError}`);
      });
      
      const cache = new InMemoryCache().restore(window.__APOLLO_STATE__);
      
      this.client = new ApolloClient({
         link: ApolloLink.from([
            middlewareLink,
            errorLink,
            httpLink,
         ]),
         cache,
         shouldBatch: true
      });
   }
}

export default new ApolloClientProvider();