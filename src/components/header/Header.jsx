import React from 'react';
import PropTypes from 'prop-types';
import './styles/header.scss';

const Header = (props) => (
   <div className="page-header">
      <div className="page-header-title"> {props.title} </div>
      <p className="page-header-desc text-muted"> {props.description} </p>
   </div>);

Header.propTypes = {
   title: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.string
   ]),
   description: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.string
   ])
};

export default Header;