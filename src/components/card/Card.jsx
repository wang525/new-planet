import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Loading from "components/loading";
import './styles/cards.scss';
import classNames from 'classnames/bind';

export default class Card extends PureComponent {
   render(){
      const { title, category, icon, children, className, loading = false } = this.props;
      const classes = classNames('card', className, { "is-loading": loading });
      
      return (<div className={classes}>
         <Loading>
            <div className="card-header">
               <span className="icon">{icon}</span>
               <span className="title">{title}</span>
               <div className="category">{category}</div>
            </div>
            <div className="card-content">
               {children}
            </div>
         </Loading>}
      </div>);
   }
}

Card.propTypes = {
   icon: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element
   ]),
   title: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element
   ]),
   category: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element
   ]),
   loading: PropTypes.bool
};
