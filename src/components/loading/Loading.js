import React, { PureComponent } from 'react';
import { Spin } from 'antd';
import PropType from 'prop-types';
import './styles/loading.scss';

const Loading = (props) =>{
   const { children, ...extraProps } = props;
   return (<Spin
      size="large"
      tip="Loading ..."
      spinning={props.loading}
      {...extraProps}>
      {!props.loading && children}
   </Spin>);
};

Loading.propType = {
   children: PropType.any.isRequired
};
export default Loading;