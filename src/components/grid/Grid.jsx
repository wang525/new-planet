import React from 'react';
import { AgGridReact } from 'ag-grid-react';
import PropTypes from 'prop-types';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

export default class Grid extends React.Component {
   render(){
      return (<div
         className="ag-theme-balham"
         style={{
            height: `${this.props.height || 600}px`,
            width: `${this.props.width + "px" || "100%"}`
         }}
      ><AgGridReact {...this.props}/></div>)
   }
};
Grid.propTypes = {
   height: PropTypes.number,
   width: PropTypes.number
};