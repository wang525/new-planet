import React from 'react';
import { Avatar, Dropdown, Menu, Icon, Button, Badge } from 'antd';
import Auth from 'lib/auth/Auth';
import './styles/navbar.scss';

export default class Navbar extends React.PureComponent {
   renderUserComponent = () =>{
      const menu = (
         <Menu>
            <Menu.Item><Icon type="user"/>Profile</Menu.Item>
            <Menu.Item><Icon type="setting"/>Settings</Menu.Item>
            <Menu.Item onClick={Auth.logout}><Icon type="logout"/>Logout</Menu.Item>
         </Menu>
      );
      
      return (<div>
         <Badge dot> <Avatar icon="user"/> </Badge>
         <Dropdown overlay={menu}>
            <Button style={{ marginLeft: "5px", border: "none", boxShadow: "none" }}>
               Jeremy Webster <Icon type="down"/>
            </Button>
         </Dropdown>
      </div>);
   };
   
   render(){
      const { history } = this.props;
      
      return (
         <div className="nav">
            <Menu mode="horizontal" className="flex flex-row">
               <div className="branding">
                  <a href="/">
                     <img src="/images/logo.png"/>
                  </a>
               </div>
               <Menu.Item eventKey={1} onClick={() => window.location.href = '/'}>
                  <Icon type="home" theme="outlined"/> Dashboard
               </Menu.Item>
               <Menu.Item eventKey={2} href="#">
                  <Icon type="shopping" theme="outlined"/> Quotes
               </Menu.Item>
               <Menu.Item eventKey={2} onClick={() => window.location.href = '/factories'}>
                  <Icon type="shopping" theme="outlined"/> Factories
               </Menu.Item>
               <Menu.Item eventKey={3} onClick={() => window.location.href = '/orders'}>
                  <Icon type="shopping" theme="outlined"/> Orders
               </Menu.Item>
               <div className="pull-left"> {this.renderUserComponent()} </div>
            </Menu>
         </div>);
   }
}