import React from 'react';
import PropTypes from 'prop-types';
import { Affix } from 'antd';
import './styles/toolbar.scss';

export default class Toolbar extends React.Component {
   static propTypes = {
      children: PropTypes.any.isRequired
   };
   
   render(){
      const { children } = this.props;
      
      return (
         <Affix offsetTop={0}>
            <div className="grid-toolbar">
               {children}
            </div>
         </Affix>);
   }
}