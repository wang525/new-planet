import React, { PureComponent } from 'react';
import { Button as BootstrapButton } from 'react-bootstrap';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import './styles/button.scss';

export default class Button extends PureComponent {   
   render(){
      const { className, loading = false, children, disabled, ...props } = this.props;
      const classes = classNames("btn", className, { "is-loading": loading });
      
      return (<BootstrapButton className={classes} disabled={disabled || loading} {...props}>
         {loading ? <span className="spinner"/> : children || " "}
      </BootstrapButton>);
   }
}

Button.propTypes = {
   children: PropTypes.any.isRequired,
   loading: PropTypes.bool,
   className: PropTypes.any,
   disabled: PropTypes.bool
};