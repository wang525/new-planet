import React from "react";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
   faAdjust,
   faAd,
   faUser,
   faBell,
   faCoins,
   faQuestion,
   faShoppingBag,
   faShoppingBasket,
   faArrowDown
   
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

library.add(
   faAd,
   faAdjust,
   faUser,
   faBell,
   faCoins,
   faQuestion,
   faShoppingBag,
   faShoppingBasket,
   faArrowDown
);

export default props => <FontAwesomeIcon {...props} />;
