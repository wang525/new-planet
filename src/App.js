import React, { Component } from "react";
import Router from "./Router";
import Auth from "lib/auth/Auth";

class App extends Component {
   constructor(props){
      super(props);
      window.auth = new Auth();
   }
   
   componentWillMount(){
      if (!localStorage.getItem("access_token") && !window.location.href.includes("login")) {
         window.location.href = "/login";
      }
   }
   
   render(){
      return (
         <div className="App">
            <Router/>
         </div>
      );
   }
}

export default App;
