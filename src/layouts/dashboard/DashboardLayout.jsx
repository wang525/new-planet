import React from 'react';
import { Container, Row } from 'reactstrap';
import Navbar from 'components/navbar/Navarbar';

export default class DashboardLayout extends React.Component {
   render(){
      const { children } = this.props;
      
      
      return (
         <Container fluid>
            <Row>
               <Navbar/>
               <div className="content"> {children} </div>
               <div className="footer">New Planet</div>
            </Row>
         </Container>
      );
   }
}