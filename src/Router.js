import React from 'react';
import { Route, Switch } from "react-router-dom";
import Authentication from './pages/authentication';
import DashboardLayout from './layouts/dashboard/DashboardLayout';
import Home from './pages/home';
import Factories from './pages/factories';
import Factory from './pages/factory';
import FactoryTemplate from './pages/factoryTemplate';
import Orders from './pages/orders';
import Order from './pages/order';


export default () => (
   //TODO: Render prop for / for authentication
   
   <Switch>
      <Route path="/login" component={Authentication}/>
      <DashboardRouter exact path="/" component={Home}/>
      <DashboardRouter exact path="/factories" component={Factories}/>
      <DashboardRouter exact path="/factories/:id" component={Factory}/>
      <DashboardRouter exact path="/factories/:id/create-excel-template" component={FactoryTemplate}/>
      <DashboardRouter exact path="/orders" component={Orders}/>
      <DashboardRouter exact path="/orders/:id" component={Order}/>
   </Switch>
);

const DashboardRouter = ({ component: Component, ...props }) =>{
   return (
      <Route {...props} render={matchProps => (
         <DashboardLayout> <Component {...matchProps}/> </DashboardLayout>
      )}/>
   );
};